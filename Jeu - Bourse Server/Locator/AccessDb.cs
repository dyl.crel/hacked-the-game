﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToolBoxNET.Connection.Database;
using ToolBoxNET.Pattern.Locator;

namespace Jeu___Bourse_Server.Locator
{
    public class AccessDb : LocatorBase
    {
        private static AccessDb _Instance;
        public static AccessDb Instance
        {
            get { return _Instance ?? (_Instance = new AccessDb()); }
        }

        private const string Provider = @"system.data.sqlclient";

        private const string CONNEXION_STRING = @"Server=DESKTOP-IRE6555\SQLEXPRESS;Database=Logdog;Trusted_Connection=True;";

        public AccessDb()
        {
            Container.Register<Connection>(Provider, CONNEXION_STRING);
        }
        public Connection Connect
        {
            get { return Container.GetInstance<Connection>(); }
        }
    }
}
