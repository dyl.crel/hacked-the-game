﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jeu___Bourse_Server.Model
{
    public class Users
    {
        public string Pseudo { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }

        public Users(string pseudo, string email, string password)
        {
            Pseudo = pseudo;
            Email = email;
            Password = password;
        }
    }
}
