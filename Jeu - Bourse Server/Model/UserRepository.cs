﻿using Jeu___Bourse_Server.Locator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToolBoxNET.Connection.Database;

namespace Jeu___Bourse_Server.Model
{
    public static class UserRepository
    {
        public static void Insert(Users Entity)
        {
            Command cmd = new Command("INSERT INTO Utilisateurs(Pseudo,Motdepasse,Email) VALUES (@Username,@Password,@Email)");
            cmd.AddParameter("Username", Entity.Pseudo);
            cmd.AddParameter("Password", Entity.Password);
            cmd.AddParameter("Email", Entity.Email);
            AccessDb.Instance.Connect.ExecuteNonQuery(cmd);
        }
    }
}
