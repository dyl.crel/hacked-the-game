﻿using Jeu___Bourse_Server.Model;
using Microsoft.AspNet.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jeu___Bourse_Server.Hubs
{
    public class GameHub : Hub
    {
        public void Register(string pseudo,string password,string email)
        {
            UserRepository.Insert(new Users(pseudo, email, password));
            Clients.Others.Register();
        }
    }
}
