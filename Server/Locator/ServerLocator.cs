﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToolBoxNET.Pattern.Locator;

namespace Server.Locator
{
    public class ServerLocator : LocatorBase
    {
        private static ServerLocator _Instance;
        public static ServerLocator Instance
        {
            get { return _Instance ?? (_Instance = new ServerLocator()); }
        }
        public ServerLocator()
        {
            Container.Register<GenerateIP>();
        }
        public GenerateIP IP
        {
            get { return Container.GetInstance<GenerateIP>(); }
        }
    }
}
