﻿using Microsoft.AspNet.SignalR;
using Server.Data;
using Server.Locator;
using Server.Model;
using Server.Model.Client.Entity;
using Server.Model.Client.Services;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToolBoxNET.Connection.Database;

namespace Server.Hubs
{
    public class GameHub : Hub
    {
        #region Propiéte
        public Users UserTemp;
        public Users UserPing;

        public Banque IBAN;

        public IEnumerable<Materiel> CPU;
        public IEnumerable<Materiel> RAM;
        public IEnumerable<Materiel> HDD;
        public IEnumerable<Materiel> GPU;
        public IEnumerable<HistoriqueBanque> Historique;
        private List<RequetCmd> _Cmd;

        public List<RequetCmd> Cmd
        {
            get { return _Cmd; }
            set { _Cmd = value; }
        }

        public RequetCmd Requete;

        public Materiel Matos;

        public int Money;

        private static ConcurrentDictionary<string, Users> GameUser = new ConcurrentDictionary<string, Users>();

        #endregion

        #region Singleton

        private static GameHub _Instance;
        public static GameHub Instance
        {
            get { return _Instance ?? (_Instance = new GameHub()); }
        }

        #endregion

        public bool UserRegister(string pseudo, string email, string password)
        {
            try
            {
                ServiceClient.Instance.Users.Register(new Users(null, pseudo, email, password, null, ServerLocator.Instance.IP.GenerateIPt(), null, null));
                return true;
            }
            catch (SqlException e) when (e.Number == 2627)
            {
                return false;
            }
        }
        public Users UserLogin(string pseudo, string password)
        {
            try
            {
                UserTemp = ServiceClient.Instance.Users.Login(pseudo, password);
                if (GameUser.Values.Select(c => c.Pseudo).Contains(pseudo))
                    UserTemp = new Users("$", null, null);
                Clients.Caller.DejaCo(UserTemp);
            }
            catch (Exception)
            {
                Clients.Caller.UserSend(UserTemp);
                return UserTemp;
            }
            if (UserTemp.Pseudo == "$")
            {
                return UserTemp;
            }
            Console.WriteLine($"L'utilisateur {UserTemp.Pseudo} vient de ce connecter");
            GameUser.TryAdd(Context.ConnectionId, UserTemp);
            Console.WriteLine($"{GameUser.Count} personnes est en ligne");
            return UserTemp;

        }
        public void AddMoney(int id, string pseudo, string op)
        {
            Matos = ServiceClient.Instance.Materiel.GetOne(id);
            UserTemp = ServiceClient.Instance.Users.Refresh(pseudo);
            IBAN = ServiceClient.Instance.Banque.Get((int)UserTemp.UserID);
            if ((IBAN.Argent -= Matos.Prix) >= 0)
            {
                ServiceClient.Instance.Banque.UpdateMoney((int)UserTemp.UserID, Matos.Prix, op);
                Money = ServiceClient.Instance.Users.Argent(pseudo);
                Clients.Caller.RefreshMoney(Money);
                ServiceClient.Instance.Materiel.Update(id);
                CPU = ServiceClient.Instance.Materiel.Get(1).Where(c => c.Stock > 0);
                RAM = ServiceClient.Instance.Materiel.Get(2).Where(c => c.Stock > 0);
                HDD = ServiceClient.Instance.Materiel.Get(3).Where(c => c.Stock > 0);
                GPU = ServiceClient.Instance.Materiel.Get(4).Where(c => c.Stock > 0);
                Clients.All.DisplayCPU(CPU);
                Clients.All.DisplayRAM(RAM);
                Clients.All.DisplayHDD(HDD);
                Clients.All.DisplayGPU(GPU);
            }
        }
        public override Task OnConnected()
        {
            return base.OnConnected();
        }

        public void DisplayMaterialShop()
        {
            CPU = ServiceClient.Instance.Materiel.Get(1).Where(c => c.Stock > 0);
            RAM = ServiceClient.Instance.Materiel.Get(2).Where(c => c.Stock > 0);
            HDD = ServiceClient.Instance.Materiel.Get(3).Where(c => c.Stock > 0);
            GPU = ServiceClient.Instance.Materiel.Get(4).Where(c => c.Stock > 0);
            Clients.All.DisplayCPU(CPU);
            Clients.All.DisplayRAM(RAM);
            Clients.All.DisplayHDD(HDD);
            Clients.All.DisplayGPU(GPU);
        }

        public void UpdateUser(string pseudo, int argent, string op)
        {
            //ServiceClient.Instance.Users.Update(pseudo, argent, op);
            UserTemp = ServiceClient.Instance.Users.Refresh(pseudo);
            ServiceClient.Instance.Banque.UpdateMoney((int)UserTemp.UserID, argent, op);
            UserTemp = ServiceClient.Instance.Users.Refresh(pseudo);
            Clients.Caller.RefreshUtilisateur(UserTemp);
            Console.WriteLine($"{pseudo} a été crédité de {argent} [AdminCmd]");
        }

        public override Task OnDisconnected(bool stopCalled)
        {
            GameUser.TryRemove(Context.ConnectionId, out UserTemp);
            return base.OnDisconnected(stopCalled);
        }
        public void IBANCall(int id)
        {
            IBAN = ServiceClient.Instance.Banque.Get(id);
            Clients.Caller.IbanSend(IBAN);
        }
        public void TransfertToCoffre(long argent, int userid)
        {
            ServiceClient.Instance.Banque.TransfertVersCoffre(argent, userid);
            IBAN = ServiceClient.Instance.Banque.Get(userid);
            Clients.Caller.IbanSend(IBAN);
            Historique = ServiceClient.Instance.HistoriqueBanque.Get(userid);
            Clients.Caller.HistoriqueSend(Historique);
        }
        public void TransfertToCompte(long argent, int userid)
        {
            ServiceClient.Instance.Banque.TransfertVersCompte(argent, userid);
            IBAN = ServiceClient.Instance.Banque.Get(userid);
            Clients.Caller.IbanSend(IBAN);
            Historique = ServiceClient.Instance.HistoriqueBanque.Get(userid);
            Clients.Caller.HistoriqueSend(Historique);
        }
        public void ChargingHistorique(int id)
        {
            Historique = ServiceClient.Instance.HistoriqueBanque.Get(id);
            Clients.Caller.HistoriqueSend(Historique);
        }
        public bool TestRequete(string query)
        {
            try
            {
                Requete = ServiceClient.Instance.Requete.Get(query);
            }
            catch (Exception)
            {
                Clients.Caller.RequetStatus(false);
                return false;
            }
            Clients.Caller.RequeteStatus(true);
            return true;
        }
        public void TestPing(string ip,string pseudo,string query)
        {
            try
            {

                UserPing = ServiceClient.Instance.Users.Ping(ip);

            }
            catch (NullReferenceException)
            {
                Instance.Cmd.Add(new RequetCmd($@"C:\Users\{pseudo}> {query} {ip}"));
                Instance.Cmd.Add(new RequetCmd($"{ip} est introuvable ou ne répond pas"));
                Clients.Caller.Pingms(Instance.Cmd);
            }
            if (UserPing != null && UserPing.Pseudo != pseudo)
            {
                int tempms;
                Random random = new Random();
                tempms = random.Next(1, 569);
                Instance.Cmd.Add(new RequetCmd($@"C:\Users\{pseudo}> {query} {ip}"));
                Instance.Cmd.Add(new RequetCmd($"{UserPing.IP} reponse = {tempms} ms"));
                tempms = random.Next(1, 569);
                Instance.Cmd.Add(new RequetCmd($"{UserPing.IP} reponse = {tempms} ms"));
                tempms = random.Next(1, 569);
                Instance.Cmd.Add(new RequetCmd($"{UserPing.IP} reponse = {tempms} ms"));
                //Clients.Caller.RequestPing(false);
                Clients.Caller.Pingms(Instance.Cmd);
                //Clients.Caller.RequestPing(true);
            }
            else
            {
                Instance.Cmd.Add(new RequetCmd($@"C:\Users\{pseudo}> {query} {ip}"));
                Instance.Cmd.Add(new RequetCmd($"Il n'est pas possible de ping sur soit même"));
                Clients.Caller.Pingms(Instance.Cmd);
            }

        }
        public void ReturnCmd()
        {
            Clients.Caller.PingMs(Instance.Cmd);
        }
        public void ReturnCmdNull()
        {
            Instance.Cmd.Clear();
            Clients.Caller.PingMs(Instance.Cmd);
        }

        public void CallSsl(string ip, string pseudo,string query)
        {
            try
            {

                UserPing = ServiceClient.Instance.Users.Ping(ip);

            }
            catch (NullReferenceException)
            {
                Instance.Cmd.Add(new RequetCmd($@"C:\Users\{pseudo}> {query} {ip}"));
                Instance.Cmd.Add(new RequetCmd($"[SSH ERROR]: Vérifier l'ip avec un ping"));
                Clients.Caller.Pingms(Instance.Cmd);
            }
            if (UserPing != null && UserPing.Pseudo != pseudo)
            {
                int tempms;
                Random random = new Random();
                tempms = random.Next(1, 569);
                Instance.Cmd.Add(new RequetCmd($@"C:\Users\{pseudo}> {query} {ip}"));
                Instance.Cmd.Add(new RequetCmd($"[SSH]: Connexion en cours"));
                //Clients.Caller.RequestPing(false);
                Clients.Caller.Pingms(Instance.Cmd);
                Clients.Caller.Hack(true);
                //Clients.Caller.RequestPing(true);
            }
            else
            {
                Instance.Cmd.Add(new RequetCmd($@"C:\Users\{pseudo}> {query} {ip}"));
                Instance.Cmd.Add(new RequetCmd($"[SSH ERROR]: Action impossible"));
                Clients.Caller.Pingms(Instance.Cmd);
            }

        }

        public void ConnectingSSL()
        {
            Random Random = new Random();
            int temprandom = Random.Next(1, 100);
            if (temprandom >= 45 && temprandom <= 65)
            {
                Clients.Caller.ConnectSSL(true);
            }
            else
            {
                Clients.Caller.ConnectSSL(false);
            }
        }


        public GameHub()
        {
            Cmd = new List<RequetCmd>();
        }

    }
}
