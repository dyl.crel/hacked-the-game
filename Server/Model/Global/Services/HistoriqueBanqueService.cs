﻿using Server.Data;
using Server.Model.Global.Entity;
using Server.Model.Global.Mapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToolBoxNET.Connection.Database;

namespace Server.Model.Global.Services
{
    public class HistoriqueBanqueService
    {
        public IEnumerable<HistoriqueBanque> Get(int id)
        {
            Command cmd = new Command("SELECT * FROM HistoriqueBanque WHERE CompteID = @ID");
            cmd.AddParameter("ID", id);
            return AccessLocator.Instance.Connexion.ExecuteReader(cmd, c => c.ToHistoriqueBanque());
        }
    }
}
