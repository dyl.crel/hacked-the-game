﻿using Server.Model.Global.Entity;
using System;
using Server.Model.Global.Mapper;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToolBoxNET.Connection.Database;
using Server.Data;

namespace Server.Model.Global.Services
{
    public class MaterielService
    {
        public IEnumerable<Materiel> Get(int categ)
        {
            Command cmd = new Command("SELECT * FROM Materiel WHERE @Cat = Categorie");
            cmd.AddParameter("Cat", categ);
            return AccessLocator.Instance.Connexion.ExecuteReader(cmd, c => c.ToMateriel());
        }
        public Materiel GetOne(int ID)
        {
            Command cmd = new Command("SELECT * FROM Materiel WHERE @id = MaterielID");
            cmd.AddParameter("id", ID);
            return AccessLocator.Instance.Connexion.ExecuteReader(cmd, c => c.ToMateriel()).SingleOrDefault();
        }

        public void Update(int ID)
        {
            Command cmd = new Command("UPDATE Materiel SET Stock -= 1 WHERE @Id = MaterielID");
            cmd.AddParameter("Id", ID);
            AccessLocator.Instance.Connexion.ExecuteNonQuery(cmd);
        }
    }
}
