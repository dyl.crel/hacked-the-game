﻿using Server.Data;
using Server.Model.Global.Entity;
using Server.Model.Global.Mapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToolBoxNET.Connection.Database;

namespace Server.Model.Global.Services
{
    public class HackService
    {
        public IEnumerable<Hack> Get(int userid)
        {
            Command cmd = new Command("SELECT * FROM Hack WHERE UserID = @id");
            cmd.AddParameter("id", userid);
            return AccessLocator.Instance.Connexion.ExecuteReader(cmd, c => c.ToHack());
        }
        public void Hacking(int userid,int hackid,int result)
        {
            Command cmd = new Command("INSERT INTO Hack(UserID,HackID,NombreHack) VALUES (@userid,@hackid,@nb) ");
            cmd.AddParameter("userid", userid);
            cmd.AddParameter("hackid", hackid);
            cmd.AddParameter("nb", result);
            AccessLocator.Instance.Connexion.ExecuteNonQuery(cmd);
        }
    }
}
