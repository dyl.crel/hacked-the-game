﻿using Server.Data;
using Server.Model.Global.Entity;
using Server.Model.Global.Mapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToolBoxNET.Connection.Database;

namespace Server.Model.Global.Services
{
    public class RequetCmdService
    {
        public RequetCmd Get(string query)
        {
            Command cmd = new Command("SELECT * FROM RequetCmd WHERE Query = @requet");
            cmd.AddParameter("requet", query);
            return AccessLocator.Instance.Connexion.ExecuteReader(cmd, c => c.ToRequetCmd()).SingleOrDefault();
        }
    }
}
