﻿using Server.Data;
using Server.Model.Global.Entity;
using Server.Model.Global.Mapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToolBoxNET.Connection.Database;

namespace Server.Model.Global.Services
{
    public class UsersService
    {
        public void Register(Users Entity)
        {
            Command cmd = new Command("INSERT INTO Utilisateurs(Pseudo,Password,Email,IP) OUTPUT INSERTED.UserID VALUES (@Pseudo,@Password,@mail,@newip)");
            cmd.AddParameter("Pseudo", Entity.Pseudo);
            cmd.AddParameter("Password", Entity.Password);
            cmd.AddParameter("mail", Entity.Email);
            cmd.AddParameter("newip", Entity.IP);
            //int temp = AccessLocator.Instance.Connexion.ExecuteScalar(cmd);
            Command cmd2 = new Command("Create_Banque", true);
            cmd2.AddParameter("VisaNumber", Banking.Instance.VisaNumber());
            cmd2.AddParameter("VisaExp", Banking.Instance.VisaExp());
            cmd2.AddParameter("VisaSecu", Banking.Instance.VisaSecu());
            cmd2.AddParameter("IBAN", Banking.Instance.IBAN());
            int temp = AccessLocator.Instance.Connexion.ExecuteScalar(cmd);
            cmd2.AddParameter("UserID", temp);
            AccessLocator.Instance.Connexion.ExecuteScalar(cmd2);
        }

        public Users Login(string pseudo,string password)
        {
            Command cmd = new Command("SELECT * FROM Utilisateurs WHERE @Pass = Password AND Pseudo = @Pseudo");
            cmd.AddParameter("Pass", password);
            cmd.AddParameter("Pseudo", pseudo);
            return AccessLocator.Instance.Connexion.ExecuteReader(cmd, c => c.ToUsers()).SingleOrDefault();
        }
        public Users Refresh(string pseudo)
        {
            Command cmd = new Command("SELECT * FROM Utilisateurs WHERE Pseudo = @Pseudo");
            cmd.AddParameter("Pseudo", pseudo);
            return AccessLocator.Instance.Connexion.ExecuteReader(cmd, c => c.ToUsers()).SingleOrDefault();
        }
        public int Argent(string pseudo)
        {
            Command cmd = new Command("SELECT Argent FROM Utilisateurs WHERE Pseudo = @Pseudo");
            cmd.AddParameter("Pseudo", pseudo);
            return AccessLocator.Instance.Connexion.ExecuteScalar(cmd);
        }

        public IEnumerable<Users> Get()
        {
            Command cmd = new Command("SELECT * FROM Utilisateurs");
            return AccessLocator.Instance.Connexion.ExecuteReader(cmd, c => c.ToUsers());
        }
        public Users Ping(string IP)
        {
            Command cmd = new Command("SELECT * FROM Utilisateurs WHERE IP = @IP");
            cmd.AddParameter("IP", IP);
            return AccessLocator.Instance.Connexion.ExecuteReader(cmd, c => c.ToUsers()).SingleOrDefault();
        }
    }
}
