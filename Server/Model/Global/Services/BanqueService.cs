﻿using Server.Data;
using Server.Model.Global.Entity;
using Server.Model.Global.Mapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToolBoxNET.Connection.Database;

namespace Server.Model.Global.Services
{
    public class BanqueService
    {
        public Banque Get(int id)
        {
            Command cmd = new Command("SELECT * FROM Banque WHERE @UserID = UserID");
            cmd.AddParameter("UserID", id);
            return AccessLocator.Instance.Connexion.ExecuteReader(cmd, c => c.ToBanque()).SingleOrDefault();
        }
        public void UpdateMoney(int userid, int argent, string op)
        {
            Command cmd = new Command($"UPDATE Banque SET Argent {op} @Argent WHERE UserID = @UserID");
            cmd.AddParameter("Argent", argent);
            cmd.AddParameter("UserID", userid);
            AccessLocator.Instance.Connexion.ExecuteNonQuery(cmd);
        }
        public void TransfertVersCoffre(long argent,int userid)
        {
            Command cmd = new Command($"UPDATE Banque SET CoffreFort += @Argent WHERE UserID = @UserID");
            cmd.AddParameter("UserID", userid);
            cmd.AddParameter("Argent", argent);
            AccessLocator.Instance.Connexion.ExecuteNonQuery(cmd);
            Command cmd2 = new Command("UPDATE Banque SET Argent -= @Argent WHERE UserID = @UserID");
            cmd2.AddParameter("Argent", argent);
            cmd2.AddParameter("UserID", userid);
            AccessLocator.Instance.Connexion.ExecuteNonQuery(cmd2);
            Command cmd3 = new Command("INSERT INTO HistoriqueBanque(CompteID,TypeOperation,TypeMessage,Argent,Destinataire) VALUES (@UserID,@TypeOperation,@Message,@Argent,@Destinataire)");
            cmd3.AddParameter("UserID", userid);
            cmd3.AddParameter("TypeOperation", "Transfert");
            cmd3.AddParameter("Message", $"Vous avez transferé {argent} vers votre coffre fort");
            cmd3.AddParameter("Argent", argent);
            cmd3.AddParameter("Destinataire", "Coffre Fort");
            AccessLocator.Instance.Connexion.ExecuteNonQuery(cmd3);
        }
        public void TransfertVersCompte(long argent, int userid)
        {
            Command cmd = new Command($"UPDATE Banque SET Argent += @Argent WHERE UserID = @UserID");
            cmd.AddParameter("UserID", userid);
            cmd.AddParameter("Argent", argent);
            AccessLocator.Instance.Connexion.ExecuteNonQuery(cmd);
            Command cmd2 = new Command($"UPDATE Banque SET CoffreFort -= @Argent WHERE UserID = @UserID");
            cmd2.AddParameter("Argent", argent);
            cmd2.AddParameter("UserID", userid);
            AccessLocator.Instance.Connexion.ExecuteNonQuery(cmd2);
            Command cmd3 = new Command("INSERT INTO HistoriqueBanque(CompteID,TypeOperation,TypeMessage,Argent,Destinataire) VALUES (@UserID,@TypeOperation,@Message,@Argent,@Destinataire)");
            cmd3.AddParameter("UserID", userid);
            cmd3.AddParameter("TypeOperation", "Transfert");
            cmd3.AddParameter("Message", $"Vous avez transferé {argent} vers votre compte bancaire");
            cmd3.AddParameter("Argent", argent);
            cmd3.AddParameter("Destinataire", "Compte Bancaire");
            AccessLocator.Instance.Connexion.ExecuteNonQuery(cmd3);
        }
    }
}
