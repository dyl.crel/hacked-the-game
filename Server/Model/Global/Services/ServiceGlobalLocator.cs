﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToolBoxNET.Pattern.Locator;

namespace Server.Model.Global.Services
{
    public class ServiceGlobalLocator : LocatorBase
    {
        private static ServiceGlobalLocator _Instance;
        public static ServiceGlobalLocator Instance
        {
            get { return _Instance ?? (_Instance = new ServiceGlobalLocator()); }
        }
        public ServiceGlobalLocator()
        {
            Container.Register<UsersService>();
            Container.Register<MaterielService>();
            Container.Register<BanqueService>();
            Container.Register<HistoriqueBanqueService>();
            Container.Register<RequetCmdService>();
            Container.Register<HackService>();
        }
        public UsersService Users
        {
            get { return Container.GetInstance<UsersService>(); }
        }
        public MaterielService Materiel
        {
            get { return Container.GetInstance<MaterielService>(); }
        }
        public BanqueService Banque
        {
            get { return Container.GetInstance<BanqueService>(); }
        }
        public HistoriqueBanqueService HistoriqueBanque
        {
            get { return Container.GetInstance<HistoriqueBanqueService>(); }
        }
        public RequetCmdService Requete
        {
            get { return Container.GetInstance<RequetCmdService>(); }
        }
        public HackService Hack
        {
            get { return Container.GetInstance<HackService>(); }
        }
    }
}
