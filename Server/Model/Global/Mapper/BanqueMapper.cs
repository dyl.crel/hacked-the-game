﻿using Server.Model.Global.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server.Model.Global.Mapper
{
    public static class BanqueMapper
    {
        public static Banque ToBanque(this IDataRecord Data)
        {
            if (Data == null) throw new ArgumentNullException();
            return new Banque()
            {
                CompteID = (int?)Data["CompteID"],
                IBAN = (string)Data["IBAN"],
                UserID = (int)Data["UserID"],
                VisaNumber = (string)Data["VisaNumber"],
                VisaExp = (string)Data["VisaExp"],
                VisaSecu = (int)Data["VisaSecu"],
                Argent = (long?)Data["Argent"],
                CoffreFort = (int)Data["CoffreFort"]
                
            };
        }
    }
}
