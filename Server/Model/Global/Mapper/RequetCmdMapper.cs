﻿using Server.Model.Global.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server.Model.Global.Mapper
{
    public static class RequetCmdMapper
    {
        public static RequetCmd ToRequetCmd(this IDataRecord Data)
        {
            if (Data == null) throw new ArgumentNullException();

            return new RequetCmd()
            {
                Query = (string)Data["Query"]
            };
        }
    }
}
