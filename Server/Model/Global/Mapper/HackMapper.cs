﻿using Server.Model.Global.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server.Model.Global.Mapper
{
    public static class HackMapper
    {
        public static Hack ToHack(this IDataRecord Data)
        {
            if (Data == null) throw new ArgumentNullException();

            return new Hack()
            {
                HackID = (int)Data["HackID"],
                NombreHack = (int)Data["NombreHack"],
                UserID = (int)Data["UserID"]
            };
        }
    }
}
