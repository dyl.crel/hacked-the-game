﻿using Server.Model.Global.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server.Model.Global.Mapper
{
    internal static class MaterielMapper
    {
        public static Materiel ToMateriel(this IDataRecord Data)
        {
            if (Data == null) throw new ArgumentNullException();

            return new Materiel()
            {
                Categorie = (int)Data["Categorie"],
                Prix = (int)Data["Prix"],
                MaterielID = (int?)Data["MaterielID"],
                Nom = (string)Data["Materiel"],
                Stock = (int)Data["Stock"]
            };
        }
    }
}
