﻿using Server.Model.Global.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server.Model.Global.Mapper
{
    internal static class UsersMapper
    {
        public static Users ToUsers(this IDataRecord Data)
        {
            if (Data == null) throw new ArgumentNullException();

            return new Users()
            {
                Pseudo = (string)Data["Pseudo"],
                Email = (string)Data["Email"],
                UserID = (int?)Data["UserID"],
                Password = (string)Data["Password"],
                IP = (string)Data["IP"],
                Niveau = (int?)Data["Niveau"],
                Admin = (bool?)Data["Admin"]
            };
        }
    }
}
