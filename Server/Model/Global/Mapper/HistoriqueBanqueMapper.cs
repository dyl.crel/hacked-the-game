﻿using Server.Model.Global.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server.Model.Global.Mapper
{
    internal static class HistoriqueBanqueMapper
    {
        public static HistoriqueBanque ToHistoriqueBanque(this IDataRecord Data)
        {
            if (Data == null) throw new ArgumentNullException();

            return new HistoriqueBanque()
            {
                CompteID = (int)Data["CompteID"],
                TypeOperation = (string)Data["TypeOperation"],
                TypeMessage = (string)Data["TypeMessage"],
                Argent = (long)Data["Argent"],
                Destinataire = (string)Data["Destinataire"]
            };
        }
    }
}
