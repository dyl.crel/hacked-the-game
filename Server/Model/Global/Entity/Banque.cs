﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server.Model.Global.Entity
{
    public class Banque
    {
        public int? CompteID { get; set; }
        public string VisaNumber { get; set; }
        public string VisaExp { get; set; }
        public int VisaSecu { get; set; }
        public string IBAN { get; set; }
        public int UserID { get; set; }
        public long? Argent { get; set; }
        public int? CoffreFort { get; set; }
    }
}
