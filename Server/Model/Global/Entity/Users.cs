﻿namespace Server.Model.Global.Entity
{
    public class Users
    {
        public int? UserID;
        public string Pseudo;
        public string Password;
        public string Email;
        public int? Argent;
        public int? Niveau;
        public string IP;
        public int? Experience;
        public bool? Admin;
    }
}
