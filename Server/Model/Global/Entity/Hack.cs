﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server.Model.Global.Entity
{
    public class Hack
    {
        public int UserID { get; set; }
        public int HackID { get; set; }
        public int NombreHack { get; set; }
    }
}
