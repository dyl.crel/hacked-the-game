﻿using Server.Model.Client.Entity;
using G = Server.Model.Global.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server.Model.Client.Mapper
{
    public static class MaterielMapper
    {
        public static Materiel ToClient (this G.Materiel Entity)
        {
            return new Materiel(Entity.MaterielID, Entity.Nom, Entity.Prix, Entity.Stock, Entity.Categorie);
        }
        public static G.Materiel ToGlobal (this Materiel Entity)
        {
            return new G.Materiel()
            {
                MaterielID = Entity.MaterielID,
                Categorie = Entity.Categorie,
                Nom = Entity.Nom,
                Prix = Entity.Prix,
                Stock = Entity.Stock
            };
        }
    }
}
