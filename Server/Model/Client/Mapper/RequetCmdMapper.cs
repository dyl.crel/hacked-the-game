﻿using Server.Model.Client.Entity;
using G = Server.Model.Global.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server.Model.Client.Mapper
{
    public static class RequetCmdMapper
    {
        public static RequetCmd ToClient(this G.RequetCmd Entity)
        {
            return new RequetCmd(Entity.Query);
        }
        public static G.RequetCmd ToGlobal(this G.RequetCmd Entity)
        {
            return new G.RequetCmd()
            {
                Query = Entity.Query
            };
        }
    }
}
