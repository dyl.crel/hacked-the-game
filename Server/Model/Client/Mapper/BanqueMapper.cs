﻿using Server.Model.Client.Entity;
using System;
using System.Collections.Generic;
using G = Server.Model.Global.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server.Model.Client.Mapper
{
    public static class BanqueMapper
    {
        public static Banque ToClient(this G.Banque Entity)
        {
            return new Banque(Entity.CompteID, Entity.VisaNumber, Entity.VisaExp, Entity.VisaSecu, Entity.IBAN, Entity.UserID,Entity.Argent,Entity.CoffreFort);
        }
        public static G.Banque ToGlobal(this Banque Entity)
        {
            return new G.Banque()
            {
                CompteID = Entity.CompteID,
                IBAN = Entity.IBAN,
                UserID = Entity.UserID,
                VisaExp = Entity.VisaExp,
                VisaNumber = Entity.VisaNumber,
                VisaSecu = Entity.VisaSecu,
                Argent = Entity.Argent,
                CoffreFort = Entity.CoffreFort
            };
        }
    }
}
