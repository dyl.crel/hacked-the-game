﻿using Server.Model.Client.Entity;
using G = Server.Model.Global.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server.Model.Client.Mapper
{
    public static class HistoriqueBanqueMapper
    {
        public static HistoriqueBanque ToClient(this G.HistoriqueBanque Entity)
        {
            return new HistoriqueBanque(Entity.CompteID, Entity.TypeOperation, Entity.TypeMessage, Entity.Argent, Entity.Destinataire);
        }
        public static G.HistoriqueBanque ToGlobal(this HistoriqueBanque Entity)
        {
            return new G.HistoriqueBanque()
            {
                CompteID = Entity.CompteID,
                Argent = Entity.Argent,
                Destinataire = Entity.Destinataire,
                TypeMessage = Entity.TypeMessage,
                TypeOperation = Entity.TypeOperation
            };
        }
    }
}
