﻿using Server.Model.Client.Entity;
using G = Server.Model.Global.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server.Model.Client.Mapper
{
    public static class UsersMapper
    {
        public static Users ToClient(this G.Users Entity)
        {
            return new Users(Entity.UserID,Entity.Pseudo,Entity.Email, Entity.Password,Entity.Niveau,Entity.IP,Entity.Experience,Entity.Admin);
        }
        public static G.Users ToGlobal(this Users Entity)
        {
            return new G.Users()
            {
                UserID = Entity.UserID,
                Email = Entity.Email,
                Password = Entity.Password,
                Pseudo = Entity.Pseudo,
                IP = Entity.IP,
                Niveau = Entity.Niveau,
                Admin = Entity.Admin,
                Experience = Entity.Experience
            };
        }
    }
}
