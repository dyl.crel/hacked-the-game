﻿using Server.Model.Client.Entity;
using G = Server.Model.Global.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server.Model.Client.Mapper
{
    public static class HackMapper
    {
        public static Hack ToClient(this G.Hack Entity)
        {
            return new Hack(Entity.UserID, Entity.HackID, Entity.NombreHack);
        }
        public static G.Hack ToGlobal(this Hack Entity)
        {
            return new G.Hack()
            {
                HackID = Entity.HackID,
                UserID = Entity.UserID,
                NombreHack = Entity.NombreHack
            };
        }
    }
}
