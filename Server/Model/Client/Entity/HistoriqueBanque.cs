﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server.Model.Client.Entity
{
    public class HistoriqueBanque
    {
        public int CompteID { get; set; }
        public string TypeOperation { get; set; }
        public string TypeMessage { get; set; }
        public long Argent { get; set; }
        public string Destinataire { get; set; }

        public HistoriqueBanque(int compt,string operation,string message,long argent, string dest)
        {
            CompteID = compt;
            TypeOperation = operation;
            TypeMessage = message;
            Argent = argent;
            Destinataire = dest;
        }
    }
}
