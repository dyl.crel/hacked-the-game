﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server.Model.Client.Entity
{
    public class Hack
    {
        public int UserID { get; set; }
        public int HackID { get; set; }
        public int NombreHack { get; set; }

        public Hack(int user,int hack,int result)
        {
            UserID = user;
            HackID = hack;
            NombreHack = result;
        }
    }
}
