﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server.Model.Client.Entity
{
    public class Materiel
    {
        public int? MaterielID { get; set; }
        public string Nom { get; set; }
        public int Prix { get; set; }
        public int Stock { get; set; }
        public int Categorie { get; set; }

        public Materiel(int? id, string nom,int prix,int stock,int categ)
        {
            MaterielID = id;
            Nom = nom;
            Prix = prix;
            Stock = stock;
            Categorie = categ;
        }
    }
}
