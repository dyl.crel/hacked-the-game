﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server.Model.Client.Entity
{
    public class Banque
    {
        public int? CompteID { get; set; }
        public string VisaNumber { get; set; }
        public string VisaExp { get; set; }
        public int VisaSecu { get; set; }
        public string IBAN { get; set; }
        public int UserID { get; set; }
        public long? Argent { get; set; }
        public int? CoffreFort { get; set; }
        internal Banque(int? compteid,string visanbr,string visaexp,int visasecu,string iban,int userid,long? argent,int? coffre)
        {
            CompteID = compteid;
            VisaNumber = visanbr;
            VisaExp = visaexp;
            VisaSecu = visasecu;
            IBAN = iban;
            UserID = userid;
            Argent = argent;
            CoffreFort = coffre;
        }
        public Banque(string visanbr, string visaexp, int visasecu, string iban, int userid):this(null,visanbr,visaexp,visasecu,iban,userid,null,null)
        {

        }
    }
}
