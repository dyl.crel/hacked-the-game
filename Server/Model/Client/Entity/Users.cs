﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server.Model.Client.Entity
{
    public class Users
    {
        public int? UserID { get; set; }
        public string Pseudo { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public int? Niveau { get; set; }
        public string IP { get; set; }
        //public int? Argent { get; set; }
        public int? Experience { get; set; }
        public bool? Admin { get; set; }

        internal Users(int? userid,string pseudo,string email,string password,int? niveau,string ip,int? xp,bool? admin)
        {
            UserID = userid;
            Pseudo = pseudo;
            Email = email;
            Password = password;
            Niveau = niveau;
            IP = ip;
            Experience = xp;
            Admin = admin;
        }
        public Users(string pseudo,string password,string ip) :this(null,pseudo,null,password,null,ip,null,null)
        {}
    }
}
