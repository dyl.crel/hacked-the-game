﻿using Server.Model.Client.Entity;
using Server.Model.Client.Mapper;
using Server.Model.Global.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server.Model.Client.Services
{
    public class MaterielService
    {
        public IEnumerable<Materiel> Get(int categ)
        {
            return ServiceGlobalLocator.Instance.Materiel.Get(categ).Select(c => c.ToClient());
        }
        public void Update(int ID)
        {
            ServiceGlobalLocator.Instance.Materiel.Update(ID);
        }
        public Materiel GetOne(int id)
        {
            return ServiceGlobalLocator.Instance.Materiel.GetOne(id).ToClient();
        }
    }
}
