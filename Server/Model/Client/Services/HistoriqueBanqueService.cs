﻿using Server.Model.Client.Entity;
using Server.Model.Client.Mapper;
using Server.Model.Global.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server.Model.Client.Services
{
    public class HistoriqueBanqueService
    {
        public IEnumerable<HistoriqueBanque> Get(int id)
        {
            return ServiceGlobalLocator.Instance.HistoriqueBanque.Get(id).Select(c => c.ToClient());
        }
    }
}
