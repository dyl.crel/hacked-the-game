﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToolBoxNET.Pattern.Locator;

namespace Server.Model.Client.Services
{
    public class ServiceClient: LocatorBase
    {
        private static ServiceClient _Instance;
        public static ServiceClient Instance
        {
            get { return _Instance ?? (_Instance = new ServiceClient()); }
        }

        public ServiceClient()
        {
            Container.Register<UsersService>();
            Container.Register<MaterielService>();
            Container.Register<BanqueService>();
            Container.Register<HistoriqueBanqueService>();
            Container.Register<RequetCmdService>();
        }

        public UsersService Users
        {
            get { return Container.GetInstance<UsersService>(); }
        }
        public MaterielService Materiel
        {
            get { return Container.GetInstance<MaterielService>(); }
        }
        public BanqueService Banque
        {
            get { return Container.GetInstance<BanqueService>(); }
        }
        public HistoriqueBanqueService HistoriqueBanque
        {
            get { return Container.GetInstance<HistoriqueBanqueService>(); }
        }
        public RequetCmdService Requete
        {
            get { return Container.GetInstance<RequetCmdService>(); }
        }
    }
}
