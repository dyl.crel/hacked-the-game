﻿using Server.Model.Client.Entity;
using Server.Model.Client.Mapper;
using Server.Model.Global.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server.Model.Client.Services
{
    public class BanqueService
    {
        public Banque Get(int id)
        {
            return ServiceGlobalLocator.Instance.Banque.Get(id).ToClient();
        }
        public void UpdateMoney(int userid, int argent, string op)
        {
            ServiceGlobalLocator.Instance.Banque.UpdateMoney(userid, argent, op);
        }
        public void TransfertVersCoffre(long argent, int userid)
        {
            ServiceGlobalLocator.Instance.Banque.TransfertVersCoffre(argent, userid);
        }
        public void TransfertVersCompte(long argent,int userid)
        {
            ServiceGlobalLocator.Instance.Banque.TransfertVersCompte(argent, userid);
        }
    }
}
