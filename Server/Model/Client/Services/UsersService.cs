﻿using Server.Model.Client.Entity;
using Server.Model.Global.Services;
using Server.Model.Client.Mapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server.Model.Client.Services
{
    public class UsersService
    {
        public void Register(Users Entity)
        {
           ServiceGlobalLocator.Instance.Users.Register(Entity.ToGlobal());
        }
        public Users Login(string pseudo,string password)
        {
            return ServiceGlobalLocator.Instance.Users.Login(pseudo, password).ToClient();
        }
        public IEnumerable<Users> Get()
        {
            return ServiceGlobalLocator.Instance.Users.Get().Select(c => c.ToClient());
        }
        public Users Refresh(string pseudo)
        {
            return ServiceGlobalLocator.Instance.Users.Refresh(pseudo).ToClient();
        }
        public int Argent(string pseudo)
        {
            return ServiceGlobalLocator.Instance.Users.Argent(pseudo);
        }
        public Users Ping(string IP)
        {
            return ServiceGlobalLocator.Instance.Users.Ping(IP).ToClient();
        }
    }
}
