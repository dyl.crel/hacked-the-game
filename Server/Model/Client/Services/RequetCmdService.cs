﻿using Server.Model.Client.Entity;
using Server.Model.Global.Services;
using Server.Model.Client.Mapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server.Model.Client.Services
{
    public class RequetCmdService
    {
        public RequetCmd Get(string query)
        {
            return ServiceGlobalLocator.Instance.Requete.Get(query).ToClient();
        }
    }
}
