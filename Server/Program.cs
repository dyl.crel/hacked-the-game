﻿using Microsoft.AspNet.SignalR;
using Microsoft.Owin.Cors;
using Microsoft.Owin.Hosting;
using Owin;
using System;
using System.Text;
using Token = System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;

namespace Server
{
    class Program
    {
        static void Main(string[] args)
        {
            var url = "http://localhost:8086/";
            using (WebApp.Start<Startup>(url))
            {
                Console.WriteLine($"Server running at {url}");
                Console.ReadLine();
            }
        }
    }

    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.UseCors(CorsOptions.AllowAll);
            app.MapSignalR();
        }
    }
}
