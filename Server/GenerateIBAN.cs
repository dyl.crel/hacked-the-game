﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server
{
    public class GenerateIBAN
    {
        public string Generateiban()
        {
            Random Number = new Random();
            int iban1, iban2, iban3, iban4;
            iban1 = Number.Next(1000, 9999);
            iban2 = Number.Next(1000, 9999);
            iban3 = Number.Next(1000, 9999);
            iban4 = Number.Next(1000, 9999);
            return $"{iban1} {iban2} {iban3} {iban4}";
        }

    }
}
