﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server
{
    public class Banking
    {
        private static Banking _Instance;
        public static Banking Instance
        {
            get { return _Instance ?? (_Instance = new Banking()); }
        }

        public string VisaNumber()
        {
            Random Number = new Random();
            int visa1, visa2, visa3, visa4;

            visa1 = Number.Next(1000, 9000);
            visa2 =  Number.Next(1000, 9000);
            visa3 = Number.Next(1000, 9000);
            visa4 = Number.Next(1000, 9000);
            return $"{visa1} {visa2} {visa3} {visa4}";
        }
        public string VisaExp()
        {
            Random Number = new Random();
            int visaexp, visaexp2;

            visaexp = Number.Next(1, 12);
            visaexp2 = Number.Next(19, 24);
            return $"{visaexp}/{visaexp2}";
        }
        public int VisaSecu()
        {
            Random Number = new Random();
            int visasecu = Number.Next(100, 999);
            return visasecu;
        }

        public string IBAN()
        {
            Random Number = new Random();
            int iban, iban2, iban3, iban4;
            iban = Number.Next(10, 99);
            iban2 = Number.Next(1000, 9999);
            iban3 = Number.Next(1000, 9999);
            iban4 = Number.Next(1000, 9999);
            return $"BE{iban} {iban2} {iban3} {iban4}";
        }

    }
}
