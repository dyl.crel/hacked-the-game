CREATE TABLE Utilisateurs(

UserID INT IDENTITY(1,1),
Pseudo VARCHAR(30) UNIQUE,
[Password] VARCHAR(56),
Email VARCHAR(200) UNIQUE,
IP VARCHAR(20),
Niveau INT DEFAULT 1,
Experience INT DEFAULT 0,
[Admin] BIT DEFAULT 0

CONSTRAINT PK_UserID PRIMARY KEY (UserID)
)
GO

CREATE PROCEDURE UP_Experience
@UserID INT,
@xp INT
AS
BEGIN

UPDATE Utilisateurs
SET Experience += @xp
WHERE @UserID = UserID

UPDATE Utilisateurs 
SET Niveau = CASE
	WHEN Experience BETWEEN 0 AND 500 THEN 1
	WHEN Experience BETWEEN 500 AND 1250 THEN 2
	WHEN Experience BETWEEN 1250 AND 2000 THEN 3
	WHEN Experience BETWEEN 2000 AND 3000 THEN 4
	WHEN Experience BETWEEN 3000 AND 4500 THEN 5
	WHEN Experience BETWEEN 4500 AND 5200 THEN 6
	WHEN Experience BETWEEN 5200 AND 6800 THEN 7
	WHEN Experience BETWEEN 6800 AND 9000 THEN 8
	WHEN Experience BETWEEN 9000 AND 11000 THEN 9
	WHEN Experience > 11000 THEN 10
	END
END



CREATE TABLE Materiel(

MaterielID INT IDENTITY(1,1),
Materiel VARCHAR(50),
Prix INT,
Categorie INT,
Stock INT DEFAULT 10

CONSTRAINT PK_MaterielID PRIMARY KEY (MaterielID)
)

INSERT INTO Materiel(Materiel,Prix,Categorie) VALUES ('I3',100,1)
INSERT INTO Materiel(Materiel,Prix,Categorie) VALUES ('I5',300,1)
INSERT INTO Materiel(Materiel,Prix,Categorie) VALUES ('I7',500,1)
INSERT INTO Materiel(Materiel,Prix,Categorie) VALUES ('4 GB',100,2)
INSERT INTO Materiel(Materiel,Prix,Categorie) VALUES ('8 GB',300,2)
INSERT INTO Materiel(Materiel,Prix,Categorie) VALUES ('16 GB', 580,2)
INSERT INTO Materiel(Materiel,Prix,Categorie) VALUES ('1 TO', 100,3)
INSERT INTO Materiel(Materiel,Prix,Categorie) VALUES ('2 TO', 200,3)
INSERT INTO Materiel(Materiel,Prix,Categorie) VALUES ('GTX 1050', 550,4)
INSERT INTO Materiel(Materiel,Prix,Categorie) VALUES ('GTX 1060', 680,4)



CREATE TABLE MaterielAchat(

UserID INT,
MaterielID INT,
Quantite INT

CONSTRAINT FK_UserID FOREIGN KEY (UserID) REFERENCES Utilisateurs(UserID),
CONSTRAINT FK_Materiel FOREIGN KEY (MaterielID) REFERENCES Materiel(MaterielID)
)

CREATE TABLE Banque(

CompteID INT IDENTITY(1,1),
VisaNumber VARCHAR(19),
VisaExp VARCHAR(5),
VisaSecu INT,
IBAN VARCHAR(50),
UserID INT,
Argent BIGINT DEFAULT 1000,
CoffreFort INT DEFAULT 0

CONSTRAINT PK_CompteID PRIMARY KEY (CompteID)
CONSTRAINT FK_UserID_Bank FOREIGN KEY (UserID) REFERENCES Utilisateurs(UserID)
)
GO
CREATE PROCEDURE Create_Banque
	@VisaNumber VARCHAR(19),
	@VisaExp VARCHAR(5),
    @VisaSecu INT,
    @IBAN VARCHAR(50),
	@UserID INT
AS
	INSERT INTO Banque(VisaExp,VisaNumber,VisaSecu,IBAN,UserID) VALUES (@VisaExp,@VisaNumber,@VisaSecu,@IBAN,@UserID)

	select * from banque

	SELECT * FROM Utilisateurs

	SELECT IBAN FROM BANQUE WHERE UserID = 1

	UPDATE Utilisateurs SET Admin = 1 WHERE Pseudo = 'disnaas'