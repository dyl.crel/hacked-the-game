﻿using Jeu___Bourse.ViewModel.Banque;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;
using ToolBoxNET.MVVM.Command;
using ToolBoxNET.Pattern.Mediator;

namespace Jeu___Bourse.ViewModel
{
    public class BanqueViewModel : RemoveSoftware
    {
        private string _Img;

        public string Img
        {
            get { return _Img; }
            set { _Img = value; RaisePropertyChanged(); }
        }

        private string _Txt;

        public string Txt
        {
            get { return _Txt; }
            set { _Txt = value; RaisePropertyChanged(); }
        }

        private string _Margin;

        public string Margin
        {
            get { return _Margin; }
            set { _Margin = value; RaisePropertyChanged(); }
        }


        private BanqueViewModel _Banking;
        public BanqueViewModel Banking
        {
            get { return _Banking; }
            set { _Banking = value; RaisePropertyChanged(); }
        }
        public BanqueViewModel()
        {
            Img = "/Img/Banque/bank.png";
            Txt = "Bienvenue à nos nouveaux Clients";
            Margin = "-142,-22,-139.333,10.333";
            Mediator<BanqueViewModel>.Instance.Register(Banque);
            DispatcherTimer Time = new DispatcherTimer();
            Time.Interval = new TimeSpan(0, 0, 10);
            Time.Tick += new EventHandler(AttributeImgAndTxt);
            Time.Start();
            //Mediator<BanqueViewModel>.Instance.Send(new BelfiusViewModel());
        }

        private ICommand _VisaBtn;
        public ICommand VisaBtn
        {
            get { return _VisaBtn ?? (_VisaBtn = new RelayCommand(ExecVisa)); }
        }

        private void ExecVisa()
        {
            Mediator<BanqueViewModel>.Instance.Send(new AccueilViewModel());
        }

        private void Banque(BanqueViewModel obj)
        {
            Banking = obj;
        }
        private void AttributeImgAndTxt(object sender,EventArgs e)
        {
            if (Img == "/Img/Banque/bitcoin.jpg" && Txt == "Le Bitcoin est encore montée")
            {
                Img = "/Img/Banque/bank.png";
                Txt = "Bienvenue à nos nouveaux Clients";
                //Margin = "-142,-22,-139.333,10.333";
            }
            else
            {
                Img = "/Img/Banque/bitcoin.jpg";
                Txt = "Le Bitcoin est encore montée";
                //Margin = "-187,-22,-184.333,-19.667";
            }
        }
    }
}
