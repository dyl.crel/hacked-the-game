﻿using Jeu___Bourse.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToolBoxNET.MVVM.Command;

namespace Jeu___Bourse.ViewModel
{
    public class ConsoleViewModel : RemoveSoftware
    {
        private Cmd _CmdRefresh;

        public Cmd CmdRefresh
        {
            get { return _CmdRefresh; }
            set { _CmdRefresh = value; }
        }

        private HackView _HackingWindow;

        public HackView HackingWindow
        {
            get { return _HackingWindow; }
            set { _HackingWindow = value; }
        }


        private bool _CanHack;

        public bool CanHack
        {
            get { return _CanHack; }
            set { _CanHack = value; }
        }


        private string _Cmd;

        public string Cmd
        {
            get
            {
                return _Cmd; ;
            }
            set
            {
                _Cmd = value
                  ; RaisePropertyChanged();
            }
        }
        private string _CheminDossier;

        public string CheminDossier
        {
            get { return _CheminDossier; }
            set { _CheminDossier = value; RaisePropertyChanged(); }
        }
        private bool _Status;

        public bool Status
        {
            get { return _Status; }
            set { _Status = value; RaisePropertyChanged(); }
        }



        private ObservableCollection<Cmd> _ListCommand;
        public ObservableCollection<Cmd> ListCommand
        {
            get { return _ListCommand; }
            set { _ListCommand = value; RaisePropertyChanged(); }
        }
        private ICommand _PushCmd;
        public ICommand PushCmd
        {
            get { return _PushCmd ?? (_PushCmd = new RelayCommand(ExecuteCmd)); }
        }
        public ConsoleViewModel()
        {
            Gamehub.PingReponse += ReceveidPingReponse;
            Gamehub.CallListCmd();
            //ListCommand.Add(new Model.Cmd($"Pour toute aide taper help"));
            CheminDossier = $@"C:\Users\{ViewModelLocator.Instance.Desktop.Pseudo}>";

        }
        private void ExecuteCmd()
        {
            string[] temp = Cmd.Split(null);
            Status = Gamehub.CallStatusQuery(temp[0]);
            if (Status == true)
            {
                //ListCommand.Add(new Model.Cmd($"{CheminDossier}{Cmd}"));
                //if (ListCommand.Count > 30)
                //{
                //    ListCommand.Clear();
                //}
                switch (temp[0])
                {
                    case "help":
                        ListCommand.Add(new Model.Cmd($"**************************************"));
                        ListCommand.Add(new Model.Cmd($"******* Liste des commandes *******"));
                        ListCommand.Add(new Model.Cmd($"**************************************"));
                        ListCommand.Add(new Model.Cmd(""));
                        ListCommand.Add(new Model.Cmd(@"ssh {ip} permet de ce connecter à une adresse ip"));
                        ListCommand.Add(new Model.Cmd(@"ping {ip} permet de vérifier qu'une adresse ip existe"));
                        ListCommand.Add(new Model.Cmd(@"clear permet de vider la console"));
                        Cmd = null;
                        break;
                    case "ssh":
                        Gamehub.Hacking += CanHackThisPlayer;
                        Cmd = null;
                        Gamehub.CallPing(temp[1], ViewModelLocator.Instance.Desktop.Pseudo, temp[0]);
                        //if (CanHack == true)
                        //{
                        ViewModelLocator.Instance.Console.HackingWindow = new HackView();
                        ViewModelLocator.Instance.Console.HackingWindow.Show();
                        //}
                        break;
                    case "ping":
                        Cmd = null;
                        Gamehub.CallPing(temp[1], ViewModelLocator.Instance.Desktop.Pseudo, temp[0]);
                        break;
                    case "clear":
                        Cmd = null;
                        Gamehub.UnCallListCmd();
                        break;
                    default:
                        break;
                }
            }
            else
            {
                ListCommand.Add(new Model.Cmd("Commande non disponible"));
                Cmd = null;
            }
        }

        private void CanHackThisPlayer(bool obj)
        {
            CanHack = obj;
        }

        private void ReceveidPingReponse(List<Cmd> obj)
        {
            App.Current.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Background, new Action(() =>
            {
                ListCommand = new ObservableCollection<Cmd>(obj);

            }));
        }

        //private void StatusReceveid(bool obj)
        //{
        //    Status = obj;
        //}
    }
}
