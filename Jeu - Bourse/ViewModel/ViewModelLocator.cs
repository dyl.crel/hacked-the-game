﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToolBoxNET.Pattern.Locator;

namespace Jeu___Bourse.ViewModel
{
    public class ViewModelLocator : LocatorBase
    {
        private static ViewModelLocator _Instance;
        public static ViewModelLocator Instance
        {
            get { return _Instance ?? (_Instance = new ViewModelLocator()); }
        }
        public ViewModelLocator()
        {
            Container.Register<Navigateur>();
            Container.Register<DesktopViewModel>();
            Container.Register<ConsoleViewModel>();
        }

        public Navigateur Navigateur
        {
            get { return Container.GetInstance<Navigateur>(); }
        }
        public DesktopViewModel Desktop
        {
            get { return Container.GetInstance<DesktopViewModel>(); }
        }
        public ConsoleViewModel Console
        {
            get { return Container.GetInstance<ConsoleViewModel>(); }
        }
    }
}
