﻿using Jeu___Bourse.Service;
using Jeu___Bourse.ViewModel.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToolBoxNET.MVVM.Base;

namespace Jeu___Bourse.ViewModel
{
    public class Navigation : ViewModelBase
    {
        private Navigation _CurrentView;
        public Navigation CurrentView
        {
            get { return _CurrentView; }
            set { _CurrentView = value; RaisePropertyChanged(); }
        }
        public void SwitchView(Navigation obj)
        {
            CurrentView = obj;
        }

        public GameHubService Gamehub;

        public DialogueService dialogService;

        public Navigation()
        {
            dialogService = new DialogueService();
            Gamehub = new GameHubService();
        }
    }
}
