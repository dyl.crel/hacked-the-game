﻿using Jeu___Bourse.ViewModel.Mediator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToolBoxNET.MVVM.Command;
using ToolBoxNET.Pattern.Mediator;

namespace Jeu___Bourse.ViewModel
{
    public class Software : DesktopViewModel
    {
        private bool _NavigateurVisibility;

        public bool NavigateurVisibility
        {
            get { return _NavigateurVisibility; }
            set { _NavigateurVisibility = value; RaisePropertyChanged(); }
        }
        private bool _BanqueVisibility;

        public bool BanqueVisibility
        {
            get { return _BanqueVisibility; }
            set { _BanqueVisibility = value; RaisePropertyChanged(); }
        }
        private bool _ConsoleVisibility;

        public bool ConsoleVisibility
        {
            get { return _ConsoleVisibility; }
            set { _ConsoleVisibility = value; RaisePropertyChanged(); }
        }

        private Software _SofwareVisibility;

        public Software SoftwareVisibilityInterne
        {
            get { return _SofwareVisibility; }
            set { _SofwareVisibility = value; }
        }



        private ICommand _MiniNavigateur;
        public ICommand MiniNavigateur
        {
            get { return _MiniNavigateur ?? (_MiniNavigateur = new RelayCommand(ExecMini)); }
        }

        private ICommand _CloseNavigateur;
        public ICommand CloseNavigateur
        {
            get { return _CloseNavigateur ?? (_CloseNavigateur = new RelayCommand(ExeClose)); }
        }
        private ICommand _MiniBanque;
        public ICommand MiniBanque
        {
            get { return _MiniBanque ?? (_MiniBanque = new RelayCommand(ExecMiniBanque)); }
        }
        private ICommand _CloseBanque;
        public ICommand CloseBanque
        {
            get { return _CloseBanque ?? (_CloseBanque = new RelayCommand(ExecCloseBanque)); }
        }
        private ICommand _CloseConsole;
        public ICommand CloseConsole
        {
            get { return _CloseConsole ?? (_CloseConsole = new RelayCommand(ExecCloseConsole)); }
        }
        private ICommand _MiniConsole;
        public ICommand MiniConsole
        {
            get { return _MiniConsole ?? (_MiniConsole = new RelayCommand(ExecMiniConsole)); }
        }

        private void ExecMiniConsole()
        {
            ConsoleVisibility = false;
        }

        private void ExecCloseConsole()
        {
            Mediator<string>.Instance.Send("Console");
            ConsoleVisibility = false;
            Mediator<RemoveSoftware>.Instance.Send(new ConsoleViewModel());
            Gamehub.UnCallListCmd();
        }

        private void ExecCloseBanque()
        {
            Mediator<string>.Instance.Send("Banque");
            //Mediator<bool>.Instance.Send(false);
            BanqueVisibility = false;
            Mediator<RemoveSoftware>.Instance.Send(new BanqueViewModel());
        }

        private void ExecMiniBanque()
        {
            BanqueVisibility = false;
        }

        private void ExeClose()
        {
            Mediator<string>.Instance.Send("Navigateur");
            NavigateurVisibility = false;
            Mediator<RemoveSoftware>.Instance.Send(new Navigateur());
            ViewModelLocator.Instance.Navigateur.Url = null;
        }

        private void ExecMini()
        {
            NavigateurVisibility = false;
        }
        public Software()
        {
            Mediator<MediatorSoftware>.Instance.Register((c) => TrueOrFalseVisibility(c));
        }

        private void TrueOrFalseVisibility(Software obj)
        {
            NavigateurVisibility = obj.NavigateurVisibility;
            ConsoleVisibility = obj.ConsoleVisibility;
            BanqueVisibility = obj.BanqueVisibility;
        }
    }
}
