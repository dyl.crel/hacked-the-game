﻿using Jeu___Bourse.ViewModel.Website;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToolBoxNET.MVVM.Command;
using ToolBoxNET.Pattern.Mediator;

namespace Jeu___Bourse.ViewModel
{
    public class Navigateur : RemoveSoftware
    {
        private string _Url;

        public string Url
        {
            get { return _Url; }
            set { _Url = value; RaisePropertyChanged(); }
        }
        private bool _VisibilityWeb;
        public bool VisibilityWeb
        {
            get { return _VisibilityWeb; }
            set { _VisibilityWeb = value; RaisePropertyChanged(); }
        }

        private Navigateur _WebSite;
        public Navigateur WebSite
        {
            get { return _WebSite; }
            set { _WebSite = value; RaisePropertyChanged(); }
        }

        private ICommand _DisplayBtn;
        public ICommand DisplayBtn
        {
            get { return _DisplayBtn ?? (_DisplayBtn = new RelayCommand(ExecBtn)); }
        }

        private void ExecBtn()
        {
            if (Url.ToLower() == "http://material.shop")
            {
                Url = Url.ToLower();
                ViewModelLocator.Instance.Navigateur.Url = Url;
                VisibilityWeb = true;
                Mediator<Navigateur>.Instance.Send(new ShopViewModel());
            }
        }
        public Navigateur()
        {

        }
        public Navigateur(string url)
        {
            Mediator<Navigateur>.Instance.Register(DisplayWebsite);
            if (url == "http://material.shop")
            {
                Url = ViewModelLocator.Instance.Navigateur.Url;
                Mediator<Navigateur>.Instance.Send(new ShopViewModel());
            }
        }
        

        private void DisplayWebsite(Navigateur obj)
        {
            WebSite = obj;
        }


    }
}
