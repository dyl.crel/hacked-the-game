﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;
using Jeu___Bourse.Model;
using ToolBoxNET.MVVM.Command;

namespace Jeu___Bourse.ViewModel.Banque
{
    public class Visa : BanqueViewModel
    {
        private string _IBAN;

        //private bool _CardVisibility;

        //public bool CardVisibility
        //{
        //    get { return _CardVisibility; }
        //    set { _CardVisibility = value; RaisePropertyChanged(); }
        //}
        //private ICommand _AfficherCard;
        //public ICommand AfficherCard
        //{
        //    get { return _AfficherCard ?? (_AfficherCard = new RelayCommand(ExecCard)); }
        //}

        //private void ExecCard()
        //{
        //    CardVisibility = true;
        //}

        public string IBAN
        {
            get { return _IBAN; }
            set { _IBAN = value; RaisePropertyChanged(); }
        }
        public Visa()
        {
            Gamehub.BankDel += AssignIban;
            Gamehub.Iban(Instance.ID);
        }

        private void AssignIban(Model.Banque obj)
        {
            App.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
            {
                IBAN = obj.IBAN;
            }));
        }
    }
}
