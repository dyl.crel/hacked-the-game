﻿using M = Jeu___Bourse.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;
using ToolBoxNET.MVVM.Command;

namespace Jeu___Bourse.ViewModel.Banque
{
    public class AccueilViewModel : BanqueViewModel
    {
        private M.Banque _Banque;

        public M.Banque BanqueAccount
        {
            get { return _Banque; }
            set { _Banque = value; }
        }


        private string _IBAN;
        public string IBAN
        {
            get { return _IBAN; }
            set { _IBAN = value; RaisePropertyChanged(); }
        }
        private long _Coffre;
        public long Coffre
        {
            get { return _Coffre; }
            set { _Coffre = value; RaisePropertyChanged(); }
        }
        private long? _Argent;
        public override long? Argent
        {
            get { return _Argent; }
            set { _Argent = value ;RaisePropertyChanged(); }
        }
        private long _SommeVersCoffre;

        public long SommeVersCoffre
        {
            get { return _SommeVersCoffre; }
            set { _SommeVersCoffre = value; RaisePropertyChanged(); }
        }

        private long _SommeVersCompte;

        public long SommeVersCompte
        {
            get { return _SommeVersCompte; }
            set { _SommeVersCompte = value; RaisePropertyChanged(); }
        }

        private ObservableCollection<M.HistoriqueBanque> _Historique;
        public ObservableCollection<M.HistoriqueBanque> Historique
        {
            get { return _Historique; }
            set { _Historique = value; RaisePropertyChanged(); }
        }

        private ICommand _VersCompte;
        public ICommand VersCompte
        {
            get { return _VersCompte ?? (_VersCompte = new RelayCommand(ExecVersCompteBtn,CanExecVersCompteBtn)); }
        }

        private bool CanExecVersCompteBtn()
        {
            if (Coffre <= 0 || (Coffre - SommeVersCompte < 0))
            {
                return false;
            }
            return true;
        }

        private void ExecVersCompteBtn()
        {
            Gamehub.VersCompteBanque(SommeVersCompte, Instance.ID);
            SommeVersCompte = 0;
        }

        private ICommand _VersCoffreBtn;
        public ICommand VersCoffreBtn
        {
            get { return _VersCoffreBtn ?? (_VersCoffreBtn = new RelayCommand(ExecCoffreBtn,CanExecCoffreBtn)); }
        }

        private bool CanExecCoffreBtn()
        {
            if ((Argent <= 0 || (Argent - SommeVersCoffre < 0)) || ((SommeVersCoffre > (Argent * 0.20))))
            {
                return false;
            }
            return true;
        }

        private void ExecCoffreBtn()
        {
            Gamehub.VersCoffreBanque(SommeVersCoffre, Instance.ID);
            SommeVersCoffre = 0;
        }

        public AccueilViewModel()
        {
            Gamehub.BankDel += AssignIban;
            Gamehub.Iban(Instance.ID);
            Gamehub.HistoriqueBanque += AssignHistorique;
            Gamehub.CallHistoriqueBanque(Instance.ID);
        }

        private void AssignHistorique(IEnumerable<M.HistoriqueBanque> obj)
        {
            App.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
            {
                Historique = new ObservableCollection<M.HistoriqueBanque>(obj);
            }));
        }

        private void AssignIban(Model.Banque obj)
        {
            App.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
            {
                BanqueAccount = obj;
                IBAN = obj.IBAN;
                Coffre = (long)obj.CoffreFort;
                Argent = obj.Argent;
            }));
        }

    }
}
