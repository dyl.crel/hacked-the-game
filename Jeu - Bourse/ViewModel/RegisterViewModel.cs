﻿using Jeu___Bourse.Model;
using Jeu___Bourse.Service;
using Jeu___Bourse.ViewModel.Service;
using Microsoft.AspNet.SignalR.Client;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;
using ToolBoxNET.MVVM.Command;
using ToolBoxNET.Pattern.Mediator;

namespace Jeu___Bourse.ViewModel
{
    public class RegisterViewModel : Navigation
    {
        private string _Pseudo;

        public string Pseudo
        {
            get { return _Pseudo; }
            set { _Pseudo = value; RaisePropertyChanged(); }
        }

        private string _Password;

        public string Password
        {
            get { return _Password; }
            set { _Password = value; RaisePropertyChanged(); }
        }
        private string _Email;

        public string Email
        {
            get { return _Email; }
            set { _Email = value; RaisePropertyChanged(); }
        }
        private ICommand _BtnRegister;
        public ICommand BtnRegister
        {
            get { return _BtnRegister ?? (_BtnRegister = new RelayCommandAsync(() => ExecBtnRegister(), (o) => CanExecBtnRegister())); }
        }
        private ICommand _BtnLogin;
        public ICommand BtnLogin
        {
            get { return _BtnLogin ?? (_BtnLogin = new RelayCommand(ExecLogin)); }
        }

        private void ExecLogin()
        {
            Mediator<Navigation>.Instance.Send(new HomeViewModel());
        }

        private async Task ExecBtnRegister()
        {
            bool x = await Gamehub.RegisterAsync(new Users(null,Pseudo, Email, Password,null,null,null,null,null));
            if (x == true)
            {
                Mediator<Navigation>.Instance.Send(new HomeViewModel());
            }
            else
            {
                dialogService.ShowNotification("Utilisateur existant");
            }
        }

        private bool CanExecBtnRegister()
        {
            return !string.IsNullOrWhiteSpace(Pseudo) && !string.IsNullOrWhiteSpace(Email) && !string.IsNullOrWhiteSpace(Password);
        }
    }
}
