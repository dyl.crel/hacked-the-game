﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using M = Jeu___Bourse.Model.Materiel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToolBoxNET.MVVM.Command;
using ToolBoxNET.Pattern.Mediator;

namespace Jeu___Bourse.ViewModel.Website
{
    public class ShopViewModel : Navigateur
    {
        private ObservableCollection<M> _CPU;
        public ObservableCollection<M> CPU
        {
            get { return _CPU; }
            set { _CPU = value; RaisePropertyChanged(); }
        }
        private ObservableCollection<M> _RAM;
        public ObservableCollection<M> RAM
        {
            get { return _RAM; }
            set { _RAM = value; RaisePropertyChanged(); }
        }
        private ObservableCollection<M> _HDD;
        public ObservableCollection<M> HDD
        {
            get { return _HDD; }
            set { _HDD = value; RaisePropertyChanged(); }
        }
        private ObservableCollection<M> _GPU;
        public ObservableCollection<M> GPU
        {
            get { return _GPU; }
            set { _GPU = value; RaisePropertyChanged(); }
        }

        private void ChargeCPU(IEnumerable<M> cpu)
        {
            App.Current.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Background, new Action(() =>
            {
                CPU = new ObservableCollection<M>(cpu);
            }));
        }
        private void ChargeGPU(IEnumerable<M> gpu)
        {
            App.Current.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Background, new Action(() =>
            {
                GPU = new ObservableCollection<M>(gpu);
            }));
        }
        private void ChargeRAM(IEnumerable<M> ram)
        {
            App.Current.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Background, new Action(() =>
            {
                RAM = new ObservableCollection<M>(ram);
            }));
        }
        public void ChargeHDD(IEnumerable<M> hdd)
        {
            App.Current.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Background, new Action(() =>
            {
                HDD = new ObservableCollection<M>(hdd);
            }));
        }
        public ShopViewModel()
        {
            Gamehub.MaterielCPU += ChargeCPU;
            Gamehub.MaterielGPU += ChargeGPU;
            Gamehub.MaterielHDD += ChargeHDD;
            Gamehub.MaterielRAM += ChargeRAM;
            Gamehub.DisplayShop();
        }
        private ICommand _BtnBuyCPU;
        public ICommand BtnBuyCPU
        {
            get { return _BtnBuyCPU ?? (_BtnBuyCPU = new RelayCommandParam((e) => ExecBtn(e), CanExecute)); }
        }

        public bool CanExecute()
        {
            if (ViewModelLocator.Instance.Desktop.Argent > 0)
            {
                return true;
            }
            return false;
        }

        public void ExecBtn(object id)
        {
            Gamehub.Money += AchatMateriel;
            Gamehub.AddArgentAsync((int)id, ViewModelLocator.Instance.Desktop.Pseudo, "-=");
        }

        private void AchatMateriel(int obj)
        {
            App.Current.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Background, new Action(() =>
            {
                ViewModelLocator.Instance.Desktop.Argent = obj;
                Mediator<int>.Instance.Send(obj);
            }));
        }

    }
}
