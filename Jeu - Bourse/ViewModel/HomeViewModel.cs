﻿using Jeu___Bourse.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using ToolBoxNET.MVVM.Command;
using ToolBoxNET.Pattern.Mediator;

namespace Jeu___Bourse.ViewModel
{
    public class HomeViewModel : Navigation
    {
        private string _Pseudo;

        public string Pseudo
        {
            get { return _Pseudo; }
            set { _Pseudo = value; RaisePropertyChanged(); }
        }
        private string _Password;

        public string Password
        {
            get { return _Password; }
            set { _Password = value; RaisePropertyChanged(); }
        }

        private ICommand _BtnLogin;
        public ICommand BtnLogin
        {
            get { return _BtnLogin ?? (_BtnLogin = new RelayCommandAsync(() => ExecLogin(), (o) => CanExecLogin())); }
        }
        private ICommand _BtnRegister;
        public ICommand BtnRegister
        {
            get { return _BtnRegister ?? (_BtnRegister = new RelayCommand(ExecRegister)); }
        }

        private void ExecRegister()
        {
            Mediator<Navigation>.Instance.Send(new RegisterViewModel());
        }

        private bool CanExecLogin()
        {
            return !string.IsNullOrEmpty(Pseudo) && !string.IsNullOrEmpty(Password);
        }

        private async Task ExecLogin()
        {
            Gamehub.DejaEnLigne += AffichageDejaCo;
            Users temp = await Gamehub.LoginAsync(Pseudo, Password);
            if (temp == null)
            {
                dialogService.ShowNotification("Pseudo et/ou mot de passe incorrect");
            }
            else if(temp.Pseudo == "$")
            {
                dialogService.ShowNotification("Connexion refusée");
                Mediator<Navigation>.Instance.Send(new HomeViewModel());
            }
            else
            {
                Mediator<Navigation>.Instance.Send(new DesktopViewModel(temp));
            }
        }
        public void AffichageDejaCo(string Data)
        {
            dialogService.ShowNotification(Data);
        }
        public event EventHandler PlayRequested;
        public HomeViewModel()
        {
            if (this.PlayRequested != null)
            {
                this.PlayRequested(this, EventArgs.Empty);
            }
            //Test Window = new Test();
            //Window.Show();
        }

    }
}
