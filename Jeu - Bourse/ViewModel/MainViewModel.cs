﻿using Jeu___Bourse.Service;
using Microsoft.AspNet.SignalR.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToolBoxNET.Pattern.Mediator;

namespace Jeu___Bourse.ViewModel
{
    public class MainViewModel : Navigation
    {
        public MainViewModel()
        {
            Mediator<Navigation>.Instance.Register(SwitchView);
            Mediator<Navigation>.Instance.Send(new HomeViewModel());
        }
    }
}
