﻿using M = Jeu___Bourse.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;
using ToolBoxNET.MVVM.Command;
using ToolBoxNET.Pattern.Mediator;
using Jeu___Bourse.ViewModel.Mediator;

namespace Jeu___Bourse.ViewModel
{
    public class DesktopViewModel : Navigation
    {
        private static DesktopViewModel _Instance;

        public static DesktopViewModel Instance
        {
            get { return _Instance ?? (_Instance = new DesktopViewModel()); }
        }

        private M.Banque _MyBank;

        public M.Banque MyBank
        {
            get { return _MyBank; }
            set { _MyBank = value; RaisePropertyChanged(); }
        }

        private M.Users _Utilisateurs;

        public M.Users Utilisateurs
        {
            get { return _Utilisateurs; }
            set { _Utilisateurs = value; }
        }


        private int _ID;

        public int ID
        {
            get { return _ID; }
            set { _ID = value; }
        }


        private string _Pseudo;

        public string Pseudo
        {
            get { return _Pseudo; }
            set { _Pseudo = value; RaisePropertyChanged(); }
        }
        private string _IP;

        public string IP
        {
            get { return _IP; }
            set { _IP = value; RaisePropertyChanged(); }
        }
        private long? _Argent;

        public virtual long? Argent
        {
            get { return _Argent; }
            set { _Argent = value; RaisePropertyChanged(); }
        }
        private int? _Niveau;

        public int? Niveau
        {
            get { return _Niveau; }
            set { _Niveau = value; RaisePropertyChanged(); }
        }
        private DateTime _Heure;
        public DateTime Heure
        {
            get { return _Heure; }
            set { _Heure = value; RaisePropertyChanged(); }
        }
        private DesktopViewModel _StartMenu;
        public DesktopViewModel StartMenu
        {
            get { return _StartMenu; }
            set { _StartMenu = value; RaisePropertyChanged(); }
        }
        private DesktopViewModel _Software;
        public DesktopViewModel Software
        {
            get { return _Software; }
            set { _Software = value; RaisePropertyChanged(); }
        }

        private DesktopViewModel _Materiel;
        public DesktopViewModel Materiel
        {
            get { return _Materiel; }
            set { _Materiel = value; RaisePropertyChanged(); }
        }

        public M.Icon CheckRemove;


        private bool _Visibility;

        public bool Visibility
        {
            get { return _Visibility; }
            set { _Visibility = value; RaisePropertyChanged(); }
        }
        private bool _SoftwareVisibility;

        public bool SoftwareVisibility
        {
            get { return _SoftwareVisibility; }
            set { _SoftwareVisibility = value; RaisePropertyChanged(); }
        }
        private bool _AdminVisibility;

        public bool AdminVisibility
        {
            get { return _AdminVisibility; }
            set { _AdminVisibility = value; RaisePropertyChanged(); }
        }

        private bool _MaterielVisibility;

        public bool MaterielVisibility
        {
            get { return _MaterielVisibility; }
            set { _MaterielVisibility = value; RaisePropertyChanged(); }
        }


        private ICommand _NavigateurBtn;
        public ICommand NavigateurBtn
        {
            get { return _NavigateurBtn ?? (_NavigateurBtn = new RelayCommand(ExecNavigateur)); }
        }
        private ICommand _BanqueBtn;
        public ICommand BanqueBtn
        {
            get { return _BanqueBtn ?? (_BanqueBtn = new RelayCommand(ExecBanque)); }
        }
        private ICommand _DossierBtn;
        public ICommand DossierBtn
        {
            get { return _DossierBtn ?? (_DossierBtn = new RelayCommand(ExecDossier)); }
        }
        private ICommand _ConsoleBtn;
        public ICommand ConsoleBtn
        {
            get { return _ConsoleBtn ?? (_ConsoleBtn = new RelayCommand(ExecConsole)); }
        }
        private ICommand _LogBtn;
        public ICommand LogBtn
        {
            get { return _LogBtn ?? (_LogBtn = new RelayCommand(ExecLog)); }
        }

        private ICommand _ConsoleCommand;
        public ICommand ConsoleCommand
        {
            get
            {
                return _ConsoleCommand ?? (_ConsoleCommand = new RelayCommand(ExeDisplayConsole));
            }
        }

        private void ExeDisplayConsole()
        {
            if (Utilisateurs.Admin == true)
            {
                if (AdminVisibility == true)
                {
                    AdminVisibility = false;
                }
                else
                {

                    AdminVisibility = true;
                }
            }
        }

        private void ExecLog()
        {
            int x = 0;
            foreach (var item in BarreTache)
            {
                if (item.Url == "/Img/page.png")
                {
                    x += 5;
                }
                else
                {
                    x += 1;
                }
            }
            if (x == BarreTache.Count)
            {
                BarreTache.Add(new M.Icon() { Nom = "Log", Url = "/Img/page.png" });
            }
        }

        private void ExecConsole()
        {
            int x = 0;
            foreach (var item in BarreTache)
            {
                if (item.Url == "/Img/cube.png")
                {
                    x += 5;
                    Mediator<Software>.Instance.Send(new ConsoleViewModel());
                    Mediator<MediatorSoftware>.Instance.Send(new MediatorSoftware()
                    {
                        BanqueVisibility = false,
                        ConsoleVisibility = true,
                        NavigateurVisibility = false
                    });
                }
                else
                {
                    x += 1;
                }
            }
            if (x == BarreTache.Count)
            {
                BarreTache.Add(new M.Icon() { Nom = "Console", Url = "/Img/cube.png" });
                Mediator<Software>.Instance.Send(new ConsoleViewModel());
                Mediator<MediatorSoftware>.Instance.Send(new MediatorSoftware()
                {
                    BanqueVisibility = false,
                    ConsoleVisibility = true,
                    NavigateurVisibility = false
                });
            }
        }

        private void ExecDossier()
        {
            int x = 0;
            foreach (var item in BarreTache)
            {
                if (item.Url == "/Img/folder.png")
                {
                    x += 5;
                }
                else
                {
                    x += 1;
                }
            }
            if (x == BarreTache.Count)
            {
                BarreTache.Add(new M.Icon() { Nom = "Dossier", Url = "/Img/folder.png" });
            }
        }

        private void ExecBanque()
        {
            int x = 0;
            foreach (var item in BarreTache)
            {
                if (item.Url == "/Img/banking.png")
                {
                    x += 5;
                    Mediator<Software>.Instance.Send(new BanqueViewModel());
                    Mediator<MediatorSoftware>.Instance.Send(new MediatorSoftware()
                    {
                        BanqueVisibility = true,
                        ConsoleVisibility = false,
                        NavigateurVisibility = false
                    });
                }
                else
                {
                    x += 1;
                }
            }
            if (x == BarreTache.Count)
            {
                BarreTache.Add(new M.Icon() { Nom = "Banque", Url = "/Img/banking.png" });
                Mediator<Software>.Instance.Send(new BanqueViewModel());
                Mediator<MediatorSoftware>.Instance.Send(new MediatorSoftware()
                {
                    BanqueVisibility = true,
                    ConsoleVisibility = false,
                    NavigateurVisibility = false
                });
            }
        }

        private void ExecNavigateur()
        {
            int x = 0;
            foreach (var item in BarreTache)
            {
                if (item.Url == "/Img/computer-1.png")
                {
                    x += 5;
                    Mediator<Software>.Instance.Send(new Navigateur(ViewModelLocator.Instance.Navigateur.Url));
                    Mediator<MediatorSoftware>.Instance.Send(new MediatorSoftware()
                    {
                        BanqueVisibility = false,
                        ConsoleVisibility = false,
                        NavigateurVisibility = true
                    });
                }
                else
                {
                    x += 1;
                }
            }
            if (x == BarreTache.Count)
            {
                BarreTache.Add(new M.Icon() { Nom = "Navigateur", Url = "/Img/computer-1.png" });
                Mediator<Software>.Instance.Send(new Navigateur(ViewModelLocator.Instance.Navigateur.Url));
                Mediator<MediatorSoftware>.Instance.Send(new MediatorSoftware()
                {
                    BanqueVisibility = false,
                    ConsoleVisibility = false,
                    NavigateurVisibility = true
                });
            }
        }

        private ObservableCollection<M.Icon> _BarreTache;
        public ObservableCollection<M.Icon> BarreTache
        {
            get { return _BarreTache ?? (_BarreTache = new ObservableCollection<M.Icon>()); }
        }

        public DesktopViewModel()
        {

        }
        public DesktopViewModel(M.Users Entity)
        {
            //ViewModelLocator.Instance.Desktop.Utilisateurs.UserID = Entity.UserID;
            Utilisateurs = Entity;
            Pseudo = Utilisateurs.Pseudo;
            IP = Utilisateurs.IP;
            Niveau = Utilisateurs.Niveau;
            Heure = DateTime.Now;
            DispatcherTimer Dt = new DispatcherTimer();
            Dt.Interval = new TimeSpan(0, 0, 1);
            Dt.Tick += (s, e) => Heure = DateTime.Now;
            Dt.Start();
            Mediator<Bureau>.Instance.Register(OpenMenu);
            Mediator<Bureau>.Instance.Send(new MenuViewModel());
            Mediator<Software>.Instance.Register(OpenSoftware);
            Mediator<RemoveSoftware>.Instance.UnRegister(RemoveSoft);
            Mediator<bool>.Instance.Register(Test);
            Mediator<string>.Instance.Register(RemoveIcon);
            Mediator<int>.Instance.Register(ChangeMoney);
            ViewModelLocator.Instance.Desktop.Pseudo = Pseudo;
            ViewModelLocator.Instance.Desktop.Argent = Argent;
            DesktopViewModel.Instance.ID = (int)Entity.UserID;
            Gamehub.BankDel += ArgentAffichage;
            Gamehub.Iban((int)Entity.UserID);
            //Mediator<Users>.Instance.Register(StockUserServer);
        }

        private void ArgentAffichage(M.Banque obj)
        {
            App.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
            {
                Argent = obj.Argent;
                ViewModelLocator.Instance.Desktop.Argent = obj.Argent;
            }
            ));
        }

        private void ChangeMoney(int obj)
        {
            App.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
            {
                Argent = obj;
            }));
        }

        public void TestRefresh(M.Users obj)
        {
            App.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
            {
                Argent = obj.Argent;
            }));
        }

        //private void StockUserServer(Users obj)
        //{
        //    AdminMod = obj;
        //}
        private void RemoveIcon(string obj)
        {
            foreach (var item in BarreTache)
            {
                if (item.Nom == obj)
                {
                    CheckRemove = item;
                }
            }
            BarreTache.Remove(CheckRemove);
        }
        private void Test(bool obj)
        {
            SoftwareVisibility = obj;
        }

        private void OpenSoftware(Software obj)
        {
            Software = obj;
        }
        private void RemoveSoft(RemoveSoftware obj)
        {
            Software = obj;
        }

        private void OpenMenu(DesktopViewModel obj)
        {
            StartMenu = obj;
        }

        private ICommand _BtnAdd;
        public ICommand BtnAdd
        {
            get { return _BtnAdd ?? (_BtnAdd = (new RelayCommand(Execute))); }
        }
        private ICommand _BtnBarreTache;
        public ICommand BtnBarreTache
        {
            get { return _BtnBarreTache ?? (_BtnBarreTache = (new RelayCommandParam((e) => ExecButton(e)))); }
        }


        private void ExecButton(object e)
        {
            switch (e)
            {
                case "Navigateur":
                    Mediator<Software>.Instance.Send(new Navigateur(ViewModelLocator.Instance.Navigateur.Url));
                    Mediator<MediatorSoftware>.Instance.Send(new MediatorSoftware()
                    {
                        BanqueVisibility = false,
                        ConsoleVisibility = false,
                        NavigateurVisibility = true
                    });
                    break;
                case "Banque":
                    Mediator<Software>.Instance.Send(new BanqueViewModel());
                    Mediator<MediatorSoftware>.Instance.Send(new MediatorSoftware()
                    {
                        BanqueVisibility = true,
                        ConsoleVisibility = false,
                        NavigateurVisibility = false
                    });
                    break;
                case "Console":
                    Mediator<Software>.Instance.Send(new ConsoleViewModel());
                    Mediator<MediatorSoftware>.Instance.Send(new MediatorSoftware()
                    {
                        BanqueVisibility = false,
                        ConsoleVisibility = true,
                        NavigateurVisibility = false
                    });
                    break;
                default:
                    break;
            }
        }

        private void Execute()
        {
            //Gamehub.Money += AjouteBigFatMoney;
            //Gamehub.AddArgentAsync(Pseudo, 500);
            if (Visibility == true)
            {
                Visibility = false;
            }
            else
            {
                Visibility = true;



            }
        }
        private void AjouteBigFatMoney(int Data)
        {
            App.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
            {
                Mediator<DesktopViewModel>.Instance.Send(new MenuViewModel());
                if (Visibility == true)
                {
                    Visibility = false;
                }
                else
                {
                    Visibility = true;



                }
                //Argent = Data;
            }));

        }
        public void RefreshMoney(M.Users entity)
        {
        }
        private string _TextCmd;

        public string TextCmd
        {
            get { return _TextCmd; }
            set { _TextCmd = value; RaisePropertyChanged(); }
        }



        private ICommand _BtnCmd;
        public ICommand BtnCmd
        {
            get { return _BtnCmd ?? (_BtnCmd = new RelayCommand(ExecCmd, CanExecCmd)); }
        }

        private bool CanExecCmd()
        {
            return !String.IsNullOrWhiteSpace(TextCmd);
        }

        private void ExecCmd()
        {
            string[] temp = TextCmd.Split(null);
            string pseudotemp;
            int argentemp;
            if (temp[0] == "addmoney")
            {
                pseudotemp = temp[1];
                argentemp = int.Parse(temp[2]);
                Gamehub.UpdatingUserAsync(pseudotemp, argentemp, "+=");
                Gamehub.Iban(Instance.ID);
            }
        }

        private ICommand _MaterielBtn;
        public ICommand MaterielBtn
        {
            get { return _MaterielBtn ?? (_MaterielBtn = new RelayCommand(ExecMateriel)); }
        }

        private void ExecMateriel()
        {
            if (MaterielVisibility == true)
            {
                MaterielVisibility = false;
            }
            else
            {
                MaterielVisibility = true;

            }
        }
    }

}


//TODO Modifier la liste (binding) pour la barredetache ainsi que rajouté la Command pour chaque bouton ainsi que sont parametre