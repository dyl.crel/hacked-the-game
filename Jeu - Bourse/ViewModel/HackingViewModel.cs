﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;
using ToolBoxNET.MVVM.Base;
using ToolBoxNET.MVVM.Command;

namespace Jeu___Bourse.ViewModel
{
    public class HackingViewModel : ConsoleViewModel
    {
        private DispatcherTimer _Timer;

        public DispatcherTimer Timer
        {
            get { return _Timer; }
            set { _Timer = value; }
        }

        private DispatcherTimer _TimerTexblock;

        public DispatcherTimer TimerTexblock
        {
            get { return _TimerTexblock; }
            set { _TimerTexblock = value; }
        }
        private bool _CanChangeHackingView;

        public bool CanChangeHackingView
        {
            get { return _CanChangeHackingView; }
            set { _CanChangeHackingView = value; }
        }


        private string _NomHack;

        public string NomHack
        {
            get { return _NomHack; }
            set { _NomHack = value; RaisePropertyChanged(); }
        }


        private int _ValueProgress;

        public int ValueProgress
        {
            get { return _ValueProgress; }
            set { _ValueProgress = value; RaisePropertyChanged(); }
        }


        private ICommand _CrackingBtn;
        public ICommand CrackingBtn
        {
            get { return _CrackingBtn ?? (_CrackingBtn = new RelayCommand(ExecCracking)); }
        }

        private void ExecCracking()
        {
            Gamehub.ResultSSL += CheckingSSL;
            Timer = new DispatcherTimer();
            Timer.Interval = new TimeSpan(0, 0, 1);
            Timer.Tick += new EventHandler(ChanginValue);
            TimerTexblock = new DispatcherTimer();
            TimerTexblock.Interval = new TimeSpan(0, 0, 1);
            TimerTexblock.Tick += new EventHandler(ChangeTextblock);
            TimerTexblock.Start();
            Timer.Start();
        }

        private void CheckingSSL(bool obj)
        {
            CanChangeHackingView = obj;
        }

        private void ChangeTextblock(object sender, EventArgs e)
        {
            switch (NomHack)
            {
                case "En cours.": NomHack = "En cours..";
                    break;
                case "En cours..": NomHack = "En cours...";
                    break;
                default: NomHack = "En cours.";
                    break;
            }
        }

        private void ChanginValue(object sender, EventArgs e)
        {
            if (ValueProgress == 100)
            {
                Gamehub.CallResultSSL();
                if (CanChangeHackingView == true)
                {
                    Timer.Stop();
                    ViewModelLocator.Instance.Console.HackingWindow.Close();
                    ValueProgress = 0;
                }
                else
                {
                    NomHack = "Tentative échouée";
                    Timer.Stop();
                    TimerTexblock.Stop();
                }
                //
                ////HackingWindow.Hide();
                //ViewModelLocator.Instance.Console.HackingWindow.Close();
                //ValueProgress = 0;
            }
            else
            {
                ValueProgress += 4;
            }
        }
        public HackingViewModel()
        {
            ValueProgress = 0;
            NomHack = "Veuiller craqué le serveur pour avoir accès";
        }
        //TODO Fermer le programme à l'arriver des 100% | Réalisé la connexion pour récuperer les données et ouvrir une nouveau bureau VM
    }   //TODO Faire un mediator bool qui permet de renvoyé justement l'information qui fera en sorte de stipuler que la fenêtre peut ce fermer
}
