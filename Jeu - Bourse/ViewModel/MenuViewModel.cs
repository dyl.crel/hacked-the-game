﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jeu___Bourse.ViewModel
{
    public class MenuViewModel : Bureau
    {
        private bool _IsActive;
        public bool IsActive
        {
            get { return _IsActive; }
            set { _IsActive = value; }
        }

        public MenuViewModel()
        {
            if (IsActive)
            {
                IsActive = true;
            }
            else
            {
                IsActive = false;
            }
        }
    }
}
