﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jeu___Bourse.Service
{
    public interface IGameHubService
    {
        event Action<string> ParticipantLoggedOut;

        void ReceiveLength(string name);
    }
}
