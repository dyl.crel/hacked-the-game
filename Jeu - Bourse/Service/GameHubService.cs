﻿using Jeu___Bourse.Model;
using Jeu___Bourse.ViewModel.Service;
using Microsoft.AspNet.SignalR.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jeu___Bourse.Service
{
    public class GameHubService 
    {
        private static GameHubService _Instance;
        public static GameHubService Instance
        {
            get { return _Instance ?? (_Instance = new GameHubService()); }
        }

        public IHubProxy Hub;
        public HubConnection Connection;

        public event Action<string> UserLog;
        public event Action<int> Money;
        public event Action<Users> RefreshUSER;
        public event Action<string> DejaEnLigne;
        public event Action<IEnumerable<Materiel>> MaterielCPU;
        public event Action<IEnumerable<Materiel>> MaterielRAM;
        public event Action<IEnumerable<Materiel>> MaterielHDD;
        public event Action<IEnumerable<Materiel>> MaterielGPU;
        public event Action<Banque> BankDel;
        public event Action<IEnumerable<HistoriqueBanque>> HistoriqueBanque;
        public event Action<bool> QueryStatus;
        public event Action<List<Cmd>> PingReponse;
        public event Action<bool> Hacking;
        public event Action<bool> ResultSSL;

        public GameHubService()
        {
            string url = "http://localhost:8086/";
            Connection = new HubConnection(url);
            Hub = Connection.CreateHubProxy("GameHub");
            Connection.Start().Wait();
        }

        public async Task<bool> RegisterAsync(Users u)
        {
           return await Hub.Invoke<bool>("UserRegister", u.Pseudo,u.Email,u.Password);
        }
        public async Task<Users> LoginAsync(string pseudo, string password)
        {
            Hub.On<string>("UserSend", (c) => UserLog?.Invoke(c));
            Hub.On<string>("DejaCo",(c) => DejaEnLigne?.Invoke(c));
            return await Hub.Invoke<Users>("UserLogin", pseudo,password);
        }
        public void AddArgentAsync(int id,string pseudo,string op)
        {
            Hub.On<int>("RefreshMoney", (c) => Money?.Invoke(c));
            Hub.Invoke<int>("AddMoney",id,pseudo,op);
        }
        public void DisplayShop()
        {
            Hub.On<IEnumerable<Materiel>>("DisplayCPU", (c) => MaterielCPU?.Invoke(c));
            Hub.On<IEnumerable<Materiel>>("DisplayRAM", (c) => MaterielRAM?.Invoke(c));
            Hub.On<IEnumerable<Materiel>>("DisplayGPU", (c) => MaterielGPU?.Invoke(c));
            Hub.On<IEnumerable<Materiel>>("DisplayHDD", (c) => MaterielHDD?.Invoke(c));
            Hub.Invoke("DisplayMaterialShop");
        }
        public void UpdatingUserAsync(string pseudo,int argent, string op)
        {
            Hub.Invoke("UpdateUser",pseudo,argent, op);
            Hub.On<Users>("RefreshUtilisateur", (c) => RefreshUSER?.Invoke(c));
        }

        public void Iban(int id)
        {
            Hub.On<Banque>("IbanSend", (c) => BankDel?.Invoke(c));
            Hub.Invoke("IBANCall", id);
        }
        public void VersCoffreBanque(long argent,int userid)
        {
            Hub.On<Banque>("IbanSend", (c) => BankDel?.Invoke(c));
            Hub.Invoke("TransfertToCoffre",argent,userid);
        }
        public void VersCompteBanque(long argent, int userid)
        {
            Hub.On<Banque>("IbanSend", (c) => BankDel?.Invoke(c));
            Hub.Invoke("TransfertToCompte", argent, userid);
        }
        public void CallHistoriqueBanque(int id)
        {
            Hub.On<IEnumerable<HistoriqueBanque>>("HistoriqueSend", (c) => HistoriqueBanque?.Invoke(c));
            Hub.Invoke("ChargingHistorique", id);
        }
        public bool CallStatusQuery(string query)
        {
            Hub.On<bool>("RequeteStatus", (c) => QueryStatus?.Invoke(c));
            return Hub.Invoke<bool>("TestRequete", query).Result;
        }
        public void CallPing(string ip,string pseudo,string query)
        {
            Hub.On<List<Cmd>>("Pingms", (c) => PingReponse?.Invoke(c));
            Hub.On<bool>("Hack", (c) => Hacking?.Invoke(c));
            switch (query)
            {
                case "ssh":
                    Hub.Invoke("CallSsl", ip, pseudo, query);
                    break;
                case "ping":
                    Hub.Invoke("TestPing", ip, pseudo, query);
                    break;
                default:
                    break;
            }
        }
        public void CallListCmd()
        {
            Hub.On<List<Cmd>>("Pingms", (c) => PingReponse?.Invoke(c));
            Hub.Invoke("ReturnCmd");
        }
        public void UnCallListCmd()
        {
            Hub.On<List<Cmd>>("Pingms", (c) => PingReponse?.Invoke(c));
            Hub.Invoke("ReturnCmdNull");
        }
        public void CallResultSSL()
        {
            Hub.On<bool>("ConnectSSL", (c) => ResultSSL?.Invoke(c));
            Hub.Invoke("ConnectingSSL");
        }
    }
}
