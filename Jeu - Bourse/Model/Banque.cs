﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jeu___Bourse.Model
{
    public class Banque
    {
        public int? CompteID { get; set; }
        public string VisaNumber { get; set; }
        public string VisaExp { get; set; }
        public int VisaSecu { get; set; }
        public string IBAN { get; set; }
        public int? UserID { get; set; }
        public long? Argent { get; set; }
        public int? CoffreFort { get; set; }

        [JsonConstructor]
        public Banque(int? compt,string visanbr,int visasecu, string visaexp,string iban,int? userid,long? argent, int? Coffre)
        {
            CompteID = compt;
            VisaNumber = visanbr;
            VisaSecu = visasecu;
            IBAN = iban;
            UserID = userid;
            Argent = argent;
            CoffreFort = Coffre;
        }
    }
}
