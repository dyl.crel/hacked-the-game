﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jeu___Bourse.Model
{
    public class Cmd
    {
        public string Query { get; set; }

        [JsonConstructor]
        public Cmd(string query)
        {
            Query = query;
        }
    }
}
