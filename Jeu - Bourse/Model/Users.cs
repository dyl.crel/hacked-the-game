﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jeu___Bourse.Model
{
    public class Users
    {
        public int? UserID { get; set; }
        public string Pseudo { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string IP { get; set; }
        public int? Argent { get; set; }
        public int? Niveau { get; set; }
        public int? Experience { get; set; }
        public bool? Admin { get; set; }

        [JsonConstructor]
        public Users(int? userid,string pseudo,string email,string password,string ip,int? argent,int? niveau,int? xp,bool? admin)
        {
            UserID = userid;
            Pseudo = pseudo;
            Email = email;
            Password = password;
            Argent = argent;
            IP = ip;
            Niveau = niveau;
            Experience = xp;
            Admin = admin;
        }
        public Users(string pseudo,string password):this(null,pseudo,null,password,null,null,null,null,null)
        { }
    }
}
