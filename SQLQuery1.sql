CREATE TABLE Utilisateurs(

UserID INT IDENTITY(1,1),
Pseudo VARCHAR(30) UNIQUE,
[Password] VARCHAR(56),
Email VARCHAR(200) UNIQUE,
Argent INT DEFAULT 1000,
IP VARCHAR(20),
Niveau INT DEFAULT 1

CONSTRAINT PK_UserID PRIMARY KEY (UserID)
)

SELECT * FROM Utilisateurs

DELETE Utilisateurs

DROP TABLE Utilisateurs

UPDATE Hacked 
SET Argent = 1200
WHERE Pseudo = 'disnaas';